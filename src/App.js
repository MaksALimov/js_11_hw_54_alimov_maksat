import PlayingField from "./components/PlayingField/PlayingField";
import React, {useState} from "react";
import Tries from "./components/Tries/Tries";
import Reset from "./components/Reset/Reset";

const fillCells = () => {
    const arrayItems = [];

    for (let i = 0; i <= 35; i++) {
        const obj = {hasItem: false, open: false};
        arrayItems.push(obj);
    }

    let randomIndex = Math.floor(Math.random() * arrayItems.length);
    arrayItems[randomIndex].hasItem = true;

    return arrayItems;
};

const App = () => {
    const [game, setGame] = useState(fillCells());

    const [counter, setCounter] = useState(0);

    const onCellClick = (index, target) => {
        target.className = 'white';
        setCounter(
            counter + 1,
        )
        if (game[index].hasItem === true) {
            game[index].open = true;
            target.innerText = 'O';
            alert('You have won!');
        }
    };

    const resetGame = () => {
        const cells = document.querySelectorAll('div.white');
        cells.forEach(cell => {
           cell.className = 'cell';
           cell.innerText = '';
        });
        setCounter(0);
        setGame(fillCells());
    }

    return (
        <div>
            <PlayingField game={game} cellClick={onCellClick}/>
            <Tries counter={counter}/>
            <Reset reset={resetGame}/>
        </div>
    );
};

export default App;
