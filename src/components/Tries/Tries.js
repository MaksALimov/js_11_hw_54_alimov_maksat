import React from 'react';

const Tries = props => {
    return (
        <div>
            <p style={{textAlign: 'center'}}>
                Tries {props.counter}
            </p>
        </div>
    );
};

export default Tries;