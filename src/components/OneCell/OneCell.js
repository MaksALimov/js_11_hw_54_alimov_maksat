import React from 'react';
import './OneCell.css'

const OneCell = props => {
    return (
        <div className='cell' onClick={props.cellClick}/>
    );
};

export default OneCell;