import React from 'react';
import './PlayingField.css';
import OneCell from "../OneCell/OneCell";

const PlayingField = props => {
    return (
        <div className="wrapper">
            {props.game.map((index, i) => {
                return <OneCell key={i} cellClick={e => props.cellClick(i, e.target)}/>
            })}
        </div>
    );
};

export default PlayingField;