import React from 'react';

const Reset = props => {
    return (
        <div style={{textAlign: 'center'}}>
            <button onClick={props.reset}>Reset</button>
        </div>
    );
};

export default Reset;